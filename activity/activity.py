# Create a Class called Camper and give it the attributes name, batch, course_type
# 2. Create a method career_track which will print out the string Currently enrolled in the <value of course_type> program.
# 3 Create a method  called info which will print out the string My name is <value of name> of batch <value of batch>.
# 4 Create an object from class Camper called zuit_camper and pass in arguments for name, batch, and course_type

class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	def details(self):
		print(f"Camper Name: {self.name}")
		print(f"Camper Batch: {self.batch}")
		print(f"Camper Course: {self.course_type}")

	def career_track(self):
		print(f"Currently enrolled in the {zuitt_camper.course_type}")

	def info(self):
		print(f"My name is {zuitt_camper.name} of batch {zuitt_camper.batch}")

zuitt_camper = Camper("Alan", "100", "python short course")

zuitt_camper.details()
zuitt_camper.info()
zuitt_camper.career_track()